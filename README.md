Установка
---------

`git clone ssh://git@bitbucket.org/dimarick/storm-link-share`
`composer install`

Генерация ссылки
----------------

Settings -> Tools -> External Tools -> +
Настраиваем
Program: php
Parameters: /path/to/folder/storm_share.php $FileRelativePath$ $SelectionStartLine$

Обновление
----------

`git pull`

`composer install`

`php storm_share.php --restart-server`