<?php

namespace Dimarick\StormLinkShare;

class SystemDetector
{
	/**
	 * @return bool
	 */
	public function isWindows()
	{
		return strncasecmp(PHP_OS, 'WIN', 3) === 0;
	}

	/**
	 * @return bool
	 */
	public function isMacos()
	{
		return strncasecmp(PHP_OS, 'Darwin', 6) === 0;
	}

	/**
	 * @return bool
	 */
	public function isLinux()
	{
		return !$this->isMacos() && !$this->isWindows();
	}
}
