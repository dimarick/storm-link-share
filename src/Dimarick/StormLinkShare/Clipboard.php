<?php
namespace Dimarick\StormLinkShare;

class Clipboard
{
	/**
	 * @var SystemDetector
	 */
	private $systemDetector;

	/**
	 * @param SystemDetector $systemDetector
	 */
	public function __construct(SystemDetector $systemDetector)
	{
		$this->systemDetector = $systemDetector;
	}

	/**
	 * @param string $text
	 */
	public function copy($text)
	{
		if ($this->systemDetector->isWindows()) {
			$clipboard = 'clip';
		} else if ($this->systemDetector->isMacos()) {
			$clipboard = 'pbclip';
		} else if ($this->systemDetector->isLinux()) {
			$clipboard = 'xclip -in -selection primary';
		} else {
			return;
		}

		$xclipProcess = proc_open($clipboard, [
			0 => ["pipe", "r"],
			1 => STDOUT,
			2 => STDERR,
		], $pipes);

		$stdin = $pipes[0];

		fwrite($stdin, $text . PHP_EOL);
		proc_close($xclipProcess);
	}
}
