#!/usr/bin/env bash

PROJECT=$1
PSTORM_PID=`ps aux | grep 'PhpStorm' | grep 'java' | grep -oP '^[^\s]+\s+\d+' | grep -oP '\d+'`
WINDOW_ID=`wmctrl -l -p | grep -F "$PSTORM_PID" | grep -F "$PROJECT" | grep -oP '^0x[0-9a-f]+'`
wmctrl -i -a $WINDOW_ID