<?php

use Dimarick\StormLinkShare\Clipboard;
use Dimarick\StormLinkShare\SystemDetector;

require_once __DIR__ . '/vendor/autoload.php';

$url = 'phpstorm://open?file=file:///' . $argv[1] . '&line=' .$argv[2];

$systemDetector = new SystemDetector();

$clipboard = new Clipboard($systemDetector);
$clipboard->copy("$url");